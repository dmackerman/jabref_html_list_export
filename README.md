Overview
--------

This is a simple jab_ref export filter to produce an html list.

Formatting is done via a css file, a sample css file is included.

Usage
-----

Before using the filter, you need to add it to JabRef (this only needs to be done once).

To add this filter to JabRef, go to Options->Manage custom exports

1. Click 'Add new' and enter the following
2. Under 'Export name', enter whatever you want to name the filter
3. Under 'Main layout file', select the 'listrefs.layout' file
4. Under 'Extension', put html
5. Click OK

Now you can export your references by going to File->Export.
In 'Files of Type', select the export name you entered above.

Configuration
-------------

If you want to highlight your name in the list of references, edit listrefs.layout and listrefs.misc.layout. to replace the string ME_MYSELF with your name as it is displayed in the references.

For example:

> If your name is shown as 'Jones B', replace 'ME_MYSELF' with 'Jones B'

This supports regex patterns. For example:

> If your name is sometimes listed as 'Jones B' and sometimes as 'Jones BK', replace 'ME_MYSELF' with 'Jones BK?'

Acknowledgment and Licensing
------------------------------

This is a heavily modified version of the filter here:
[http://www.markschenk.com/tools/jabref/](http://www.markschenk.com/tools/jabref/)

It it released under the same terms as the original:
[Creative Commons Attribution License](http://creativecommons.org/licenses/by/3.0/)


