// heavily modified version of original layout this is
// Copyright (c) 2006-2011, Mark Schenk
//
// This software is distributed under a Creative Commons Attribution 3.0 License
// http://creativecommons.org/licenses/by/3.0/
//

function toggleInfo(articleid, info) {
	var entry = document.getElementById(articleid);
	var abs = document.getElementById('abs_'+articleid);
	var bib = document.getElementById('bib_'+articleid);
	
	if (abs && info == 'abstract') {
    abs.classList.toggle("noshow")
	} else if (bib && info == 'bibtex') {
    bib.classList.toggle("noshow")
	} else { 
		return;
	}

  // if either abstact or bibtex is visible, highlight the entry
  if ((abs && !abs.classList.contains("noshow")) || // abstract exists and is visible
      (bib && !bib.classList.contains("noshow"))) { // bibtex exists and is visible
		entry.classList.add("bib_highlight");
  } else { // neither abstract nor bibtex is visible, remove the highlighting
	  entry.classList.remove("bib_highlight");
  }
}

